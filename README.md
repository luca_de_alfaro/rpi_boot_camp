# Raspberry PI Boot Camp Examples
Luca de Alfaro, 2016

## Before you start

Before you start with these boot camps, you need to install some software on your raspberry pi.  Log into it, and type the following commands:

    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get install git

Then, go to the directory of the boot camp you want to follow, and read the README file there: 

- Webcam: 

    cd webcam

